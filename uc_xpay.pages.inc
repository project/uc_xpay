<?php

/**
 * @file
 * gestpay menu items.
 *
 */

//include "GestPayCrypt.inc.php";

function uc_xpay_complete($cart_id = 0) {
	_uc_process_response(FALSE);
}

function uc_xpay_server() {
	_uc_process_response(TRUE);
}

function uc_xpay_error() {
	watchdog('xpay', 'ERROR, received REPONSE !response for payment, TERMINAL_ID: !terminal_id, TRANSACTION_ID: !transaction_id', array('!terminal_id' => trim($_POST["TERMINAL_ID"]), '!transaction_id' => trim($_POST["TRANSACTION_ID"]),'!response' => trim($_POST["RESPONSE"])),WATCHDOG_ERROR);
	drupal_set_message(t("Payment cancelled or an error has occurred. Response from payment gateway: !response",array('!response' => trim($_POST["RESPONSE"])) ));
	drupal_goto('cart');
	#return $output = t("Error with payment gateway")." RESPONSE: "..", TERMINAL_ID: "..", TRANSACTION_ID: ".;
}

function uc_xpay_finalize() {
  if (!$_SESSION['do_complete']) {
    drupal_goto('cart');
  }
  $order = uc_order_load(arg(3));
  // Add a comment to let sales team know this came in through the site.
  uc_order_comment_save($order->order_id, 0, t('Order created through website.'), 'admin');
  $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));

  watchdog('xpay', 'ORDER FINALIZED, order #!order_id', array('!order_id'=>$order_id),WATCHDOG_NOTICE);

  $page = variable_get('uc_cart_checkout_complete_page', '');
  if (!empty($page)) {
    drupal_goto($page);
  }
  //$output = uc_order_status_data($order->order_status, 'state');
  return $output;
}


